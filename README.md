# Summary

Source: https://gitlab.com/agrozyme-docker/traefik

Traefik, The Cloud Native Edge Router

# Environment Variables

When you start the image, you can adjust the configuration of the instance by passing one or more environment variables
on the docker run command line.

## TRAEFIK_CONFIG_FILE

- The variable is optional, set the configuration file path.
- The default value is `/etc/traefik/traefik.yaml`

## TRAEFIK_ACME_FILE

- The variable is optional, set the configuration file path.
- The default value is `/etc/traefik/acme.json`
