# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Traefik
      module Build
        def self.main
          System.run('apk add --no-cache traefik')
        end
      end

      module Run
        def self.update_acme
          acme = ENV.fetch('TRAEFIK_ACME_FILE', '/etc/traefik/acme.json')

          Shell.make_folders(File.dirname(acme))

          if not File.exist?(acme)
            FileUtils.touch(acme)
          end

          Shell.change_owner(acme)
          Shell.change_mode(0600, acme)
        end

        def self.main
          config = ENV.fetch('TRAEFIK_CONFIG_FILE', '/etc/traefik/traefik.yaml')

          Shell.update_user
          System.invoke('Update ACME', self.method(:update_acme))
          Shell.make_folders(File.dirname(config))
          System.execute('traefik', { configFile: config }, '')
        end
      end
    end
  end
end
